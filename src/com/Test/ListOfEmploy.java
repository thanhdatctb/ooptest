package com.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ListOfEmploy {
    private int soLuong; //So lượng nhân viên
    private List<Employ> Employs;

    public ListOfEmploy(int soLuong, List<Employ> employs) {
        this.soLuong = soLuong;
        Employs = employs;
    }

    public ListOfEmploy() {
        this.Employs = new ArrayList<Employ>();
    }

    public ListOfEmploy nhap(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập số lượng: ");

        int soLuong = scanner.nextInt();

        for(int i = 0; i <soLuong; i ++){
            System.out.println("Nhập thông tin nhân viên thứ "+i+":");
            Scanner scanner1 = new Scanner(System.in);
            System.out.print("nhân viên này tăng ca không (y/n): ");
            String option = scanner1.nextLine();
            switch (option){
                case "y":
                    Employ_OT employ_ot = new Employ_OT().nhap();
                    this.Employs.add(employ_ot);
                    break;
                default:
                    Employ employ = new Employ().nhap();
                    this.Employs.add(employ);
                    break;
            }
        }
        return  new ListOfEmploy(soLuong, this.Employs);
    }
    public void hienThi(){
        System.out.println("Số lượng nhân viên: "+this.soLuong);
        System.out.println("Nhân viên trong danh sách");
        this.Employs.forEach(s->s.hienThi());
        System.out.println("tổng lương nhân viên: " + TinhLuong());
    }
    public float TinhLuong(){
        return (float) this.Employs.stream().mapToDouble(s-> s.TinhLuong()).sum();
    }


}
