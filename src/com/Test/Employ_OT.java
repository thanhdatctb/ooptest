package com.Test;

import java.util.Scanner;

public class Employ_OT extends  Employ{
    private float soGio;

    public float getSoGio() {
        return soGio;
    }

    public void setSoGio(float soGio) {
        this.soGio = soGio;
    }

    public Employ_OT(String maNV, String tenNV, float soNgayCong, float soGio) {
        super(maNV, tenNV, soNgayCong);
        this.soGio = soGio;
    }
    public Employ_OT nhap(){
        Employ employ = super.nhap();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Số giờ OT: ");
        int soGio = scanner.nextInt();
        return new Employ_OT(employ.getMaNV(), employ.getTenNV(), employ.getSoNgayCong(), soGio);
    }
    public void hienThi(){
        super.hienThi();
        System.out.println("Số OT: "+this.soGio);
    }

    @Override
    public float TinhLuong() {
        return super.TinhLuong() + soGio * 50000;
    }

    public Employ_OT() {
    }
}
