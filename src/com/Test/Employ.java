package com.Test;

import java.util.Scanner;

public class Employ {
    private String MaNV;
    private String tenNV;
    private float soNgayCong;

    public float TinhLuong(){
        return this.soNgayCong*250000;
    }
    public Employ(String maNV, String tenNV, float soNgayCong) {
        MaNV = maNV;
        this.tenNV = tenNV;
        this.soNgayCong = soNgayCong;
    }

    public Employ() {
    }

    public Employ nhap(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Mã nhân viên: ");
        String maNv = scanner. nextLine();
        System.out.print("Tên nhân viên: ");
        String tenNv = scanner.nextLine();
        System.out.print("Số ngày công: ");
        float soNgayCong = scanner.nextInt();
        return new Employ(maNv,tenNv,soNgayCong);
    }
    public  void hienThi(){
        System.out.println("Mã nhân viên: "+this.MaNV);
        System.out.println("Tên nhân viên: "+this.tenNV);
        System.out.println("Số ngày lương: "+this.soNgayCong);
        System.out.println("Tính lương: "+this.TinhLuong());
    }
    public String getMaNV() {
        return MaNV;
    }

    public void setMaNV(String maNV) {
        MaNV = maNV;
    }

    public String getTenNV() {
        return tenNV;
    }

    public void setTenNV(String tenNV) {
        this.tenNV = tenNV;
    }

    public float getSoNgayCong() {
        return soNgayCong;
    }

    public void setSoNgayCong(float soNgayCong) {
        this.soNgayCong = soNgayCong;
    }
}
