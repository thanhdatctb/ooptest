package com.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Bill {
    private String tenKH; //Tên khách hàng
    private List<String> items;

    public Bill(String tenKH, List<String> items) {
        this.tenKH = tenKH;
        this.items = items;
    }
    public  Bill(){
        this.items = new ArrayList<String>();
    }

    public String getTenKH() {
        return tenKH;
    }

    public void setTenKH(String tenKH) {
        this.tenKH = tenKH;
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }

    public Bill nhap(){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Tên khách hàng: ");
        String tenKH = scanner. nextLine();
        System.out.print("Số phần từ hóa đơn:");
        List<String> items = new ArrayList<String>();
        int count = scanner.nextInt();
        for(int i = 1; i <= count; i ++){
            Scanner scanner1 = new Scanner(System.in);
            System.out.print("mặt hàng thứ "+i+": ");
            String item = scanner1.nextLine();
            items.add(item);
        }
        return  new Bill(tenKH,items);
    }
    public void hienThi(){
        System.out.println("Tên khách hàng: "+this.tenKH);
        System.out.println("Các mặt hàng mua: ");
        items.forEach(s-> {
            System.out.println(s);
        });
    }
}
